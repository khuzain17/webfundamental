#Web Jurusan TE UB Sederhana

#disusun untuk memenuhi tugas akhir pelatihan Belajar Fundamental Front-End Web Development

>Oleh Muhammad Khuzain
>Dibuat pada bulan April 2021 di Banjarbaru

petunjuk penggunaan:

1. import folder Root dengan menggunakan aplikasi editor seperti visual studio

2. install pakage yg diperlukan

3. jalankan perintah "npm run build" atau "npm run "start-dev"

4. maka akan muncul folder "dist" yg berisi file hasil build. 

5. menuju dist > index.html dan jalankan "Go Live" untuk melihat hasil build dari web ini

6. tutup aplikasi editor dan browser apabila tidak digunakan

7. terima kasih

note: 
--code dari web ini (.html .js .css) mungkin akan terdapat kemiripan dengan code pembelajaran atau yg lain. 
--Namun sudah saya coba ubah sesuai dengan kebutuhan.
--mohon untuk dimaklumi sebab keterbatasan waktu ( deadline bdd kemenkraf) dan keahlian saya yg terbatas (baru belajar membuat web)
--salam untuk tim dicoding

