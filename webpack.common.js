const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
   entry: "./src/app.js",
   output: {
       path: path.resolve(__dirname, "dist"),
       filename: "bundle.js"
   },
   module: {
       rules: [
           {
               test: /\.css$/,
               use: [
                   {
                       loader: "style-loader"
                   },
                   {
                       loader: "css-loader"
                   }
               ]
           },
           {
           rules: [
            {
              test: /\.html$/i,
              loader: 'html-loader',
              options: {
                esModule: false,
              },
            },
          ],
            },  
            {
            test: /\.(jpg|png)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        
                        outputPath: 'img/',
                        publicPath: 'img/',
                        esModule: false
                    }
                }
            ]
        }
           

       ]
   },
   plugins: [
       new HtmlWebpackPlugin({
           template: "./src/index.html",
           filename: "index.html"
       }),
       new CleanWebpackPlugin()
   ]
}
