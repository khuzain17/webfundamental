import "./styles/style.css";/**import style css */


import "./script/component/app-bar.js"; /**import app-bar js */
import "./script/component/search-bar.js"; /**import search-bar js */
import "./script/component/footer-bar.js"; /**import footer bar js */
import "./script/component/map-bar.js";/**import map-bar js */

import "./script/component/bagian-gambar.js";/**import bagian-gambar js */
import "./script/component/bagian-prodi.js";/**import bagian-prodi js */
import "./script/component/bagian-slider.js";/**import bagian-slider js */
import "./script/component/bagian-navbar.js";/**import bagian-navbar js */
import "./script/data/api-korona.js";/**import api-korona js */


import main from "./script/view/main.js"; /**import main js */
document.addEventListener("DOMContentLoaded", main);

main();