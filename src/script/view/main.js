import FilterMatkul from '../data/filter-matkul.js'; /* import filter matkul*/
import '../component/search-bar.js'; /*memanggil komponen searchbar ke main js*/
import Typed from '../data/typed.js'; /*impor typed*/
import 'SmoothScroll';

const main = () => {
    const searchElement = document.querySelector("search-bar"); /**untuk mengembalikan  elemen dalam dokumen yang cocok selector CSS yang ditentukan */
     const matkulListElement = document.querySelector("#matkulList");
  
    const kliktombol = () => { /**fungsi untuk bagian search klik */
        FilterMatkul.searchMatkul(searchElement.value)
            .then(renderResult)
            .catch(fallbackResult)
    };
  
    const renderResult =  results => {
        matkulListElement.innerHTML = "";
        results.forEach(matkul => {
            const {name, sks, prasyaratmatkul, prasyaratsks, isimateri} = matkul; /**menggabung  name, sks, prasyaratmatkul, prasyaratsks, isimateri menjadi 1 yakni matkul*/
            const matkulElement = document.createElement("div");
            matkulElement.setAttribute("class", "matkul");
  
            matkulElement.innerHTML = `
                <div class="matkul-info">
                    <h2>Mata Kuliah : <strong> ${name} </strong> (${sks} sks)</h2> 
                    <p>Prasyarat : ${prasyaratmatkul} dan ${prasyaratsks}</p>
                    <p><strong>Isi materi </strong>: ${isimateri}</p>
                </div>`; /**bagian yg akan ditampilkan pada hasil pencarian */
  
            matkulListElement.appendChild(matkulElement);
        })
    };
  
    const fallbackResult = message => {
        matkulListElement.innerHTML = "";
        matkulListElement.innerHTML += `<h2 class="placeholder">${message}</h2>`; /**hasil pencarian */
    };
    searchElement.clickEvent = kliktombol;


    const baseUrl = 'https://covid19.mathdro.id/api/countries/Indonesia/'; /**mendefinisikan url yg akan dipanggil  */
    const getData = () => {
       fetch(baseUrl)
           .then(response => response.json()) 
           .then(results => { 
               const hasilreport = document.querySelector('api-korona'); /**agar data dapat diakses oleh 'api-korona'*/
               hasilreport.coronaItem = results;
           })
           .catch(error => warningerror()); /**intruksi jika tidak dapat memanggil url */
   }
   
   const warningerror = (message = 'Periksa koneksi internet...') =>{
       alert(message); /**menampilkan alert jika terjadi putus koneksi*/
   }
   getData();

var scroll = new SmoothScroll('a[href*="#"]', {
    speed: 500,
    speedAsDuration: true
    });
   
   
 };

const typed = new Typed("#typed", {
    strings:["<strong>Join</strong><strong class='primary'> ELEKTRO UB !!!</strong>", "<strong>be </strong><strong class='primary'>THE BEST !!!</strong>", "<strong>we </strong><strong class='primary'>NEED YOU !!!</strong>"], /**isi dari string yang akan ditampilkan pada .slider */
    typespeed:40, /**kecepatan dari fungsi typed */
    loop:true, /**perintah agar intruksi typed berulang terus menerus */
});



 export default main; /**memberikan keluaran agar dapat dipanggil di app.js */