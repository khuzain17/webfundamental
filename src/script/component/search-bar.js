import find from '../../img/find.png';

class SearchBar extends HTMLElement { /**inisialisasi clas search bar */
   connectedCallback(){
       this.render();
   }
  
   set clickEvent(event) { /**pengaturan uuntuk adanya kejadian klik button */
       this._clickEvent = event;
       this.render();
   }
 
   get value() {
       return this.querySelector("#searchElement").value;
   }
 
   render() {
       this.innerHTML = ` 
       <div>
       <div id="search-container" class="search-container">
           <input placeholder="Search Courses" id="searchElement" type="search">
           <button id="searchButtonElement" type="submit"><img src="./img/find.png" height="30px"></button>
       </div>
       </div>
       `; /**bagian yg dieksekusi atau bagian yg mewakiliki script pada file html */
 
       this.querySelector("#searchButtonElement").addEventListener("click", this._clickEvent);
   }
}
 
customElements.define("search-bar", SearchBar); /**melakukan define dari custom elemen sehingga nilai search-bar = SearchBar */