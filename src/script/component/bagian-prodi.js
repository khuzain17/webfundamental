class BagianProdi extends HTMLElement { /* inisialisasi class BagianProdi*/
    connectedCallback(){
        this.render();
    }
    render() {
        this.innerHTML = ` 
        <div>
                    <div class="section-title text-center wow zoomIn">
                        <h2><b>PROGRAM STUDI SARJANA</b></h2>
                        <p>
                            Penjurusan (konsentrasi) di program studi Sarjana Teknik Elektro UB
                        </p>
                    </div><br>
        <div class="container">
            <div class="row wow bounceInUp">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-wrap text-center">
                        <h3>TEKNIK TEGANGAN TINGGI (A)</h3>
                        <P>
                            Berfokus pada teori dan penerapan tenaga listrik dari pembangkitan hingga distribusi.
                        </P>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-wrap text-center">
                        <h3>TEKNIK ELEKTRONIKA (B)</h3>
                        <P>
                            Berfokus pada pembelajaran perancangan rangkaian elektronika (arus lemah).
                        </P>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-wrap text-center">
                        <h3>TEKNIK TELEKOMUNIKASI (C)</h3>
                        <P>
                            Berfokus pada teori dan penerapan telekomunikasi serta aspek legalitas hukum.
                        </P>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-wrap text-center">
                        <h3>TEKNIK KONTROL <br> (D)</h3>
                        <P>
                            Berfokus pada sistem teknik kontrol dan penggunaannya seperti arduino.
                        </P>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-wrap text-center">
                        <h3>TEKNIK REKAYASA KOMPUTER (E)</h3>
                        <P>
                            Berfokus pada teknik perekayasaan komputer seperti paralel komputer.
                        </P>
                    </div>
                </div>
            </div>
        </div> 
        </div><br>
        `; /* html bagian BagianProdi */
    }
 }
  
 customElements.define("bagian-prodi", BagianProdi);