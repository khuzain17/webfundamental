class AppBar extends HTMLElement { /* inisialisasi class AppBar*/
    connectedCallback(){
        this.render();
    }
    render() {
        this.innerHTML = `<h2>Course Searching</h2>`; /* Bagian informasi pembuka pada bagian header */
    }
 }
  
 customElements.define("app-bar", AppBar); /**melakukan define dari custom elemen sehingga nilai app-bar = AppBar */