class FooterBar extends HTMLElement { /* inisialisasi class footer bar*/
    connectedCallback(){
        this.render();
    }
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    connectedCallback(){
        this.render();
    }

    render() {
        this.shadowDOM.innerHTML = ` 
        <style>
        footer
        {
            background-color: black;
            color: white;
            background-size: cover;
            background-attachment: fixed;
            padding-top: 20px;
            padding-bottom: 20px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
        }
        .section-title
        {
            text-align: center !important;
        }
        </style>
        <footer><div>
                <div class="section-title">
                    <h2><b>TEKNIK ELEKTRO UB</b></h2>
                    <p>
                        &copy; 2021. By Muhammad Khuzain for TEUB.
                    </p>
                </div>
        </footer></div>
        `; /* Bagian html footer bar */
    }
 }
  
 customElements.define("footer-bar", FooterBar);



 