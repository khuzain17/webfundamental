import gambar1 from '../../img/1.png'; /* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar2 from '../../img/2.png'; /* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar3 from '../../img/3.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar4 from '../../img/4.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar5 from '../../img/5.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar6 from '../../img/6.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar7 from '../../img/7.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */
import gambar8 from '../../img/8.png';/* jangan dihapus! fungsi: agar code dengan .png terdeteksi oleh file-loader dan html loader */

class BagianGambar extends HTMLElement { /* inisialisasi class gambar*/
    connectedCallback(){
        this.render();
    }
    render() {
        this.innerHTML = ` 
        <div >
            <div>
                <div class="section-title">
                    <h2><b>GALLERIES</b></h2>
                    <p>
                        Foto lingkungan jurusan Teknik Elektro UB
                    </p>
                </div>
            </div><br>
            <div class="container-fluid">
                <div class="row no-gutters">
                <div class="col-md-3 col-sm-3 col-xs-3  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
                    <div class="img-wrapper">
                        <a href="./img/1.png" title="Foto tampak depan Teknik ELektro Gedung B (Baru)">
                            <img src="./img/1.png" class="img-responsive">
                        </a>
                    </div>
                    
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp"data-wow-duration=".5s" data-wow-delay=".5s">
                    <div class="img-wrapper">
                        <a href="./img/2.png" title="Foto tampak depan Teknik ELektro Gedung B (Lama)">
                            <img src="./img/2.png" class="img-responsive">
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp"data-wow-duration=".5s" data-wow-delay=".5s">
                    <div class="img-wrapper">
                        <a href="./img/3.png" title="Foto tampak depan Gedung Himpunan (Lama)">
                            <img src="./img/3.png" class="img-responsive">
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp"data-wow-duration=".5s" data-wow-delay=".5s">
                    <div class="img-wrapper">
                        <a href="./img/4.png" title="Foto tampak depan Teknik ELektro Gedung A (Baru)">
                            <img src="./img/4.png" class="img-responsive">
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp">
                    <div class="img-wrapper">
                        <a href="./img/5png" title="Foto tampak belakang Teknik ELektro Gedung B (Lama)">
                            <img src="./img/5.png" class="img-responsive">
                        </a>
                    </div>
                    
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp">
                    <div class="img-wrapper">
                        <a href="./img/6.png" title="Foto lantai 2 Teknik ELektro Gedung A (Lama)">
                            <img src="./img/6.png" class="img-responsive">
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp">
                    <div class="img-wrapper">
                        <a href="./img/7.png" title="Foto dari atas ke parkiran Teknik ELektro Gedung A (Lama)">
                            <img src="./img/7.png" class="img-responsive">
                        </a>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3 col-xs-3 wow fadeInUp">
                    <div class="img-wrapper">
                        <a href="./img/8.png" title="Foto tampak belakang Teknik ELektro Gedung A (Lama)">
                            <img src="./img/8.png" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        `; /* Bagian html gambar */
    }
 }
  
 customElements.define("bagian-gambar", BagianGambar);