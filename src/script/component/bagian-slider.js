class BagianSlider extends HTMLElement { /* inisialisasi class bagian slider*/
    connectedCallback(){
        this.render();
    }
    render() {
        this.innerHTML = ` 
        <div class="slider-overlay">
        <div class="slider-content">
            <div class="typed"> </div>
            <div><h1> <span id="typed"></span></h1></div>
            <div class="cta-div">
                <a href="https://selma.ub.ac.id/" class="btn1">LET'S JOIN US !</a>
                <a href="https://ub.ac.id/" class="btn2">LEARN TODAY !</a>
            </div>
            <br><br>
            <div class="social-networks">
                <a href="https://web.facebook.com/Universitas.Brawijaya.Official/?_rdc=1&_rdr" class="fa fa-facebook"></a>
                <a href="https://twitter.com/UB_Official?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" class="fa fa-twitter"></a>
                <a href="https://id.linkedin.com/school/university-of-brawijaya/" class="fa fa-linkedin"></a>
                <a href="https://www.instagram.com/univ.brawijaya/" class="fa fa-instagram"></a>
            </div>
        </div>
        </div>
        `; /* Bagian html dari slider */
    }
 }
  
 customElements.define("bagian-slider", BagianSlider);