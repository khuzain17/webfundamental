class BagianNavbar extends HTMLElement { /* inisialisasi class NavBar*/
    connectedCallback(){
        this.render();
    }
    render() {
        this.innerHTML = ` 
        <div class="container">
        <div class="navbar-header">
            <!--------Responsive Button------->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navi">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-log animated bounceInDown"><img src="./img/logoUB.png" width="50px"></div>
        </div>
        <div class="collapse navbar-collapse" id="navi">
            <!---Navigation menus-->
        <ul class="nav navbar-nav navbar-right animated bounceInDown">
            <li><a href="#slider">Home</a></li>
            <li><a href="#prodi">prodi</a></li>
            <li><a href="#courses">Courses</a></li>
            <li><a href="#galeri">Galleries</a></li>
            <li><a href="#map">Map</a></li>
            <li><a href="https://siam.ub.ac.id/" class="login">Login</a></li>
        </ul>
        <!--navigation menus end-->
        </div>
        </div> 
        `; /* Bagian html navbar */
    }
 }
  
 customElements.define("bagian-navbar", BagianNavbar);