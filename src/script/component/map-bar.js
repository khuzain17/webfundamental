class MapBar extends HTMLElement { /* inisialisasi class MapBar*/
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    connectedCallback(){
        this.render();
    }
    render() {
        this.shadowDOM.innerHTML = ` 
                <style>
                .dmaps
                {
                    margin: auto;
                    width: 90%;
                    max-width: 100%;
                    margin: auto;
                }
                </style>
                <div class="dmaps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11671.056059256129!2d112.61356697447604!3d-7.957831321569217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78827f2d620975%3A0xf19b7459bbee5ed5!2sBrawijaya%20University!5e0!3m2!1sen!2sid!4v1618534356131!5m2!1sen!2sid" width=100% height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>
                `; /* Bagian html map bar */
    }
 }
  
 customElements.define("map-bar", MapBar);