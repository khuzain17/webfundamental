jQuery(document).ready(function(){
    "use strict"
    $('.slider').ripples({
        dropRadius: 13,
        perturbance: 0.01,
      });

      $(".text").typed({
          strings:["<strong>Join</strong><strong class='primary'> ELEKTRO UB !!!</strong>", "<strong>be </strong><strong class='primary'>THE BEST !!!</strong>", "<strong>we </strong><strong class='primary'>NEED YOU !!!</strong>"],
          typespeed:0,
          loop:true,
      });

      $(window).scroll(function(){

        var top = $(window).scrollTop();
        if (top>=60){
            $("nav").addClass('secondary');
        }

        else
          if($("nav").hasClass('secondary')){
              $("nav").removeClass('secondary');
          }

        });
        $('a').smoothScroll();
        new WOW().init();
    });