const matkuls = [
    {
        name: "Fisika",
        sks: "4",
        prasyaratmatkul: "Tidak ada",
        prasyaratsks: "0 sks",
        isimateri: "Mekanika Pendahuluan, Kinematika partikel, Dinamika partikel, Kerja dan Energi, Dinamika Rotasi, Listrik magnit, Medan Elektrik, Poternsial elektrik, Arus elektrik, Medan magnit, EMF terinduksi."
    },
    {
        name: "Matematika",
        sks: "4",
        prasyaratmatkul: "Tidak ada",
        prasyaratsks: "0 sks",
        isimateri: "Himpunan, Relasi dan Fungsi, Kombinatorial, Induksi Matematik, Graf, Pohon, Kompleksitas Algoritma, Fungsi Numerik Diskrit dan Pembangkit, Group, Ring, Field."
    },
    {
        name: "Bahasa Inggris",
        sks: "3",
        prasyaratmatkul: "Tidak ada",
        prasyaratsks: "0 sks",
        isimateri: "Efficient and effective reading strategies, translation, writing skills, essays writing.",
    },
    {
        name: "Kewirausahaan",
        sks: "2",
        prasyaratmatkul: "Tidak ada",
        prasyaratsks: "100 sks",
        isimateri: "Pengantar Kewirausahaan, Analisis Ekonomi dan  Keuangan, Total Quality Management (TQM), Mutu Sebagai Alternatif Peningkatan Daya Saing, Pengambilan Keputusan, Kepemimpinan dan Kerja sama Tim, Nilai-nilai dan Etika Kewirausahaan, Manfaat Ekonomi Standar, Standar dan Inovasi, Cakupan standar, Anatomi standar dan prinsip dasar pengembangan standar, Pengembangan standar, Sistem penerapan standar, Prinsip-prinsip metrologi dan penilaian kesesuaian.",
    },
    {
        name: "Elektronika",
        sks: "4",
        prasyaratmatkul: "Tidak ada",
        prasyaratsks: "20 sks",
        isimateri: "Teori semikonduktor, diode dan rangkaian diode, Pembiasan BJT dan pembiasan FET, Pengenalan thyristor, Analisis dasar Op-Amp, Analisis sinyal kecil penguat transistor BJT dan FET, Penguat bertahapan jamak (multistage amplifier), Pengaturan tegangan dan stabilitas termal, Penguat umpan balik dan osilator serta penguat daya."
    }
];

export default matkuls; /**agar data pada modul ini dapat dieksport ketika dipanggil pada fungsi lain*/