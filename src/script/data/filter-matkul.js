import matkuls from './matkuls.js'; /**import modul js lain */

class FilterMatkul {
    static searchMatkul(keyword) {
        return new Promise((resolve, reject) => {
            const filteredMatkuls = matkuls.filter(
                matkul => matkul.name.toUpperCase().includes(
                    keyword.toUpperCase()
                    )
                );
            if (filteredMatkuls.length) 
            {
                resolve(filteredMatkuls); /**bagian yg dieksekusi ketika sistem menemukan bagian yg dicari */
            } 
            else 
            {
                reject(`${keyword} tidak terdapat pada database!"`); /**bagian yg dieksekusi ketika sistem tidak menemukan bagian yg dicari */
            }
        });
    }
}

export default FilterMatkul; /**agar nilai fungsi dapat dieksport menuju fungsi lain*/