class ApiKorona extends HTMLElement{
    constructor(){
        super();
        this.shadowDom = this.attachShadow({mode:'open'});
    }

    /**
     * @param {any} item
     */
    set coronaItem(item){
        this._coronaItem = item;
        this.render();
    }

    render(){
        this.shadowDom.innerHTML = `
                                    <style>
                                        @media (max-width:991px){
                                            .tabelkorona{
                                                width:100%;
                                                padding:0px;
                                                
                                            }
                                        }
                                        .section-title
                                        {
                                            text-align: center !important;
                                        }
                                        .tabelkorona {
                                            font-family: sans-serif;
                                            color: #132c33;
                                            border-collapse: collapse;
                                            width: 80%;
                                            border: 1px solid #f2f5f7;
                                            position: relative;
                                            margin: auto;
                                            font-size: 17px !important;
                                        }
                                        
                                        .tabelkorona tr th{
                                            background: #126e82;
                                            color: #fff;
                                            font-weight: bold;
                                        }
                                        
                                        .tabelkorona, th, td {
                                            padding: 0px 0px;
                                            text-align: center;
                                        }
                                        
                                        .tabelkorona tr:hover {
                                            background-color: #D8E3E7;
                                        }
                                        
                                        .tabelkorona tr:nth-child(odd) {
                                            background-color: #D8E3E7;
                                        }
                                    </style>
                                    <div>
                                        <div>
                                            <div class="section-title text-center">
                                            <h2><b>UPDATE KORONA INDONESIA</b></h2>
                                            <p>
                                                Berisi update harian jumlah korban Covid-19 
                                            </p>
                                        </div><br>
                                    </div>

                                        <div>
                                        <table class="tabelkorona text-center">
                                        <tr>
                                            <th><h4>No</h4></th>
                                            <th><h4>Kriteria</h4></th>
                                            <th><h4>Jumlah</h4></th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Terkonfirmasi</td>
                                            <td><h4>${this._coronaItem.confirmed.value}</h4></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Sembuh</td>
                                            <td><h4>${this._coronaItem.recovered.value}</h4></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Meninggal</td>
                                            <td><h4>${this._coronaItem.deaths.value}</h4></td>
                                        </tr>
                                    
                                    </table>
                                    </div>
                                    </div>`;
    }
}

customElements.define('api-korona',ApiKorona);